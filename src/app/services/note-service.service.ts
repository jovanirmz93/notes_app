import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { INoteData } from "../interfaces/note.interface";

@Injectable({
  providedIn: 'root'
})
export class NoteServiceService {

  constructor(
    private http: HttpClient
  ) { }


  getNotes() {
    return this.http.get<INoteData[]>('../../assets/notes.json');
  }
}
