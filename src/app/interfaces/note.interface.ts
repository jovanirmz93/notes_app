import { IUser } from "./user.interface";

export interface INoteData {
    title: string,
    content: string,
    date: string,
    name: string
}