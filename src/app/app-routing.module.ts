import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CreateNoteComponent } from './components/create-note/create-note.component';
import { CreateUserComponent } from './components/create-user/create-user.component';
import { NotesComponent } from './components/notes/notes.component';

const routes: Routes = [
  { path: 'notes', component: NotesComponent },
  { path: 'createNote', component: CreateNoteComponent },
  { path: 'createUSer', component: CreateUserComponent },
  { path: '**', pathMatch: 'full', redirectTo: 'notes' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
