import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { INoteData } from 'src/app/interfaces/note.interface';
import { NoteServiceService } from 'src/app/services/note-service.service';

@Component({
  selector: 'app-notes',
  templateUrl: './notes.component.html',
  styleUrls: ['./notes.component.scss']
})
export class NotesComponent implements OnInit {

  public note: INoteData[] = [];

  constructor(
    private notes: NoteServiceService,
    private route: Router
  ) { }

  ngOnInit(): void {
    this.Notes();
  }

  Notes() {
    this.notes.getNotes().subscribe(data => {
      this.note = data;
    });
  }

  editNote() {
    this.route.navigate(['./createNote']);
  }

}
