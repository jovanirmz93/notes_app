import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { IUser } from 'src/app/interfaces/user.interface';
import { UsersService } from 'src/app/services/users.service';

@Component({
  selector: 'app-create-user',
  templateUrl: './create-user.component.html',
  styleUrls: ['./create-user.component.scss']
})
export class CreateUserComponent implements OnInit {

  public formUser: FormGroup;
  public saveUser: IUser;
  public user: IUser[] = [];

  constructor(
    private fb: FormBuilder,
    private userService: UsersService
  ) {
    this.formNewUser();
  }

  ngOnInit(): void {

    this.getUsers();

  }

  formNewUser(): void {
    this.formUser = this.fb.group({
      name: ['', Validators.required]
    });
  }

  createUser() {
    const newUser: IUser = this.formUser.value;
    console.log(newUser);
    localStorage.setItem('user', JSON.stringify(newUser));
  }

  getUsers() {
    this.userService.getUsers().subscribe(data => {
      console.log(data);
      this.user = data;
    });
  }

}
