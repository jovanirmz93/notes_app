import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { INoteData } from 'src/app/interfaces/note.interface';

@Component({
  selector: 'app-create-note',
  templateUrl: './create-note.component.html',
  styleUrls: ['./create-note.component.scss']
})
export class CreateNoteComponent implements OnInit {

  public formNote: FormGroup;

  public saveNote: INoteData;

  constructor(
    private fb: FormBuilder
  ) {
    this.formN();
  }


  ngOnInit(): void {
  }


  formN(): void {
    this.formNote = this.fb.group({
      user: ['', Validators.required],
      title: ['', [Validators.required]],
      content: ['', [Validators.required]],
      date: ['', [Validators.required]],
    });
  }


  save() {
    const note: INoteData = this.formNote.value;
    console.log(note);
    localStorage.setItem('note', JSON.stringify(note));
  }



}
